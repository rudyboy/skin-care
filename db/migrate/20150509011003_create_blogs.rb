class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.text :content
      t.string :title
      t.string :content_short
      t.string :image

      t.timestamps null: false
    end
  end
end

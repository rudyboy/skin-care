class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :profile
      t.string :image
      t.integer :product_id

      t.timestamps null: false
    end
  end
end

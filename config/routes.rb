Rails.application.routes.draw do

  get 'materials/new'

  get 'materials/edit'

  root 'pages#home'

  resources :pages, only: [] do
    collection do
      get 'home'
      get 'about'
      get 'contract'
    end
  end

  resources :blogs

  resources :users do
    collection do
      get 'login'
      post 'auth'
      delete 'logout'
    end
  end

  resources :products do
    resources :materials
  end
end

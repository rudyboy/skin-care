class ProductsController < ApplicationController
  # load_and_authorize_resource param_method: :product_params

  def index
    @products = Product.all
  end

  def show
    # @product = Product.where(id: params[:id]).first
    # @materials = @product.materials
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product
    else
      render 'new'
    end
  end

  def edit
    @product = Product.where(id: params[:id]).first
  end

  def update
    @product = Product.where(id: params[:id]).first
    if @product.update_attributes(product_params)
      redirect_to @product
    else
      render 'edit'
    end
  end

  def destroy
    product = Product.where(id: params[:id]).first
    product.destroy if product

    redirect_to products_path
  end

private
  def product_params
    params.require(:product).permit(:name, :title, :price, :profile, :image)
  end

end

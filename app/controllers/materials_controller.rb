class MaterialsController < ApplicationController
  def new
    @product = Product.find_by(id: params[:product_id])
    @material = Material.new
  end

  def create
    @material = Material.new(material_params)
    if @material.save
     redirect_to product_path(@material.product.id)
    else
      @product = Product.find_by(id: params[:product_id])
      render :new
    end
  end

  def edit
    @product = Product.find_by(id: params[:product_id])
    @material = Material.find_by(id: params[:id])
  end

  def update
    @product = Product.find_by(id: params[:product_id])
    @material = Material.find_by(id: params[:id])

    if @material.update_attributes(material_params)
      redirect_to @product
    else
      render 'edit'
    end
  end

  def destroy
    @material = Material.find_by(id: params[:id])
    @material.destroy
    @product = Product.find_by(id: params[:product_id])
    redirect_to @product
  end

private
  def material_params
    params.require(:material).permit(:product_id, :image, :profile)
  end
end

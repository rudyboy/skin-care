class UsersController < ApplicationController

  def login
    @user = User.new
  end

  def logout
    reset_session
    redirect_to root_path
  end

  def auth
    if user = User.auth(params[:user])
      session[:id] = user.id
      hashids = Hashids.new(user.email)
      redirect_to user_path(hashids.encode(user.id))
    else
      @user = User.new(user_params)
      render 'login'
    end
  end

  def show
    @user = current_user

    redirect_to root_path if @user.nil?
  end

  def edit
    @user = current_user

    redirect_to root_path if @user.nil?
  end

  def update
    @user = current_user
    if @user && @user.update_attributes(user_update_params)
      hashids = Hashids.new(@user.email)
      redirect_to user_path(hashids.encode(@user.id))
    else
      render 'edit'
    end
  end

private
  def user_params
    params.require(:user).permit(:email, :password)
  end

  def user_update_params
    params.require(:user).permit(:email, :password, :username)
  end

end

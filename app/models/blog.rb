class Blog < ActiveRecord::Base

  mount_uploader :image, ImageUploader

  validates :title, :image, :content, :content_short, presence: true
end

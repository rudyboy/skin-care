require 'digest/sha2'
class User < ActiveRecord::Base

  validates_presence_of :username
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :password
  validates_length_of :password, minimum: 6


  enum role: [:admin, :common]
  before_save :password_digest, :email_downcase

private
  def password_digest
    self.password = User.auth_password(password)
  end

  def email_downcase
    self.email = email.downcase
  end

  def self.auth_password(password)
    Digest::SHA1.hexdigest('skin-care-key-rudy' + password)
  end

  def self.auth(params)
    User.where(email: params[:email].downcase,
               password: auth_password(params[:password])).first
  end
end

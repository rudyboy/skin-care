class Material < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  belongs_to :product

  validates_presence_of :product_id
  validates_presence_of :profile
  validates_presence_of :image
end

class Ability
  include CanCan::Ability

  def initialize(user)
      user ||= User.new
      if user.role == 'admin'
        can :manage, :all
      else
        can :read, Product
      end
    #
    # here are :read, :create, :update and :destroy.
    #
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    #
    #   can :update, Article, :published => true
  end
end

module ApplicationHelper
  def hashids(base, key)
    Hashids.new(base).encode(key)
  end

  def is_active?(num)
    actions = ['home', 'products', 'blogs', 'about', 'contract']
    return 'active' if actions[num] == controller_name
    return 'active' if actions[num] == action_name
  end
end
